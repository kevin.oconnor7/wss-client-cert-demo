## Setup

   1. Run `yarn install`
   2. Generate dummy certificate:
      `openssl req -nodes -new -x509 -keyout key.pem -out cert.pem`
   3. Start server: `node index.js`
   4. Visit `https://localhost:5001` and bypass the cert error by typing `thisisunsafe`.
   5. Visit `https://localhost:5000` and bypass the cert error by typing `thisisunsafe`.
   6. Provide any client certificate when requested. Any cert is accepted.
   7. You should see: `failed to connect to websocket!'`
   8. Refresh the page the websocket should connect with the message:
      `websocket connected!`.
