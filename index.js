const fs = require('fs');
const https = require('https');
const url = require('url');
const WebSocket = require('ws');

const WS_PORT = 5001;
const HTTP_PORT = 5000;

const CERT_FILE = `cert.pem`;
const KEY_FILE = `key.pem`;
const httpsConfig = {
  key: fs.readFileSync(KEY_FILE),
  cert: fs.readFileSync(CERT_FILE),
};
const wsHttpsConfig = Object.assign({}, httpsConfig, {
  requestCert: true, 
  rejectUnauthorized: false,
});

const isAuthorized = (request) => {
  const peerCert = request.socket.getPeerCertificate();
  if (!peerCert.subject) {
    console.log(`Rejecting connection with missing certificate.`);
    return false;
  }
  console.log(`Received connection with cert: ${peerCert.subject.CN}`);
  return true;
};

const wsHttpsServer = https.createServer(wsHttpsConfig, (request, response) => {
  if (!isAuthorized(request)) {
    response.writeHead(400);
    response.end();
    return;
  }
  response.writeHead(200);
  response.end();
}).listen(WS_PORT, () => {
  console.log(`Websocket listening on ${WS_PORT}`);
});
const wsServer = new WebSocket.Server({ noServer: true });
wsHttpsServer.on('upgrade', (request, socket, head) => {
  const pathname = url.parse(request.url).pathname;
  
  if (pathname !== '/ws') {
    socket.destroy();
    return;
  }

  if (!isAuthorized(request)) {
    console.log('unauthorized upgrade');
    socket.destroy();
    return;
  }
  wsServer.handleUpgrade(request, socket, head, (ws) => {
    wsServer.emit('connection', ws, request);
  });
});
wsServer.on('connection', (ws, req) => {
  console.log(`Incoming connection from: ${req.connection.remoteAddress}`);

  ws.on('close', () => {
    console.log(
      `Closed connection with ${req.connection.remoteAddress}`);
  });
});

const indexHtml = new Promise((resolve, reject) => {
  fs.readFile('index.html', 'utf8', (err, file) => {
    if (err) {
      reject(err);
    } else {
      resolve(file);
    }
  });
});

https.createServer(httpsConfig, (request, response) => {
  // Just always serve index.html.
  fs.readFile('index.html', 'utf8', (err, file) => {
    if (err) {
      response.writeHead(500, { 'Content-Type': 'text/plain' });
      response.end();
      return;
    }
    response.writeHead(200);
    response.write(file, 'utf8');
    response.end();
  });
}).listen(HTTP_PORT, () => {
  console.log(`Webserver listening on ${HTTP_PORT}`);
});
